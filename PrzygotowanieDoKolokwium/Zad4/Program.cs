﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad4
{
    class Program
    {
        static void Main(string[] args)
        {
            B b = new B();
            A a = new A();
            A c = new C();
            I i = new I();
            I k = new D();
            J j = new E();
            E d = new D();

            Console.WriteLine(c is B);
            Console.WriteLine(i is J);
            Console.WriteLine(b is A);
            Console.WriteLine(d is A);
            Console.WriteLine(d is E);
            Console.WriteLine(k is E);
            Console.WriteLine(c is I);

            Console.ReadKey();
        }
    }
}
