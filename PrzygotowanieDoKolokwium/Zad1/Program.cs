﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1
{
    /*
    Zdefiniuj klasę Student i Grupa (grupa ma nazwę, ma też tablicę studentów). Dla obu klas zaimplementuj konstruktory i metodę ToString
    (ma ona zwracać string z zawartością obiektu klasy). Dla grupy zdefiniuj metody DodajStudenta i UsunStudenta. Następnie proszę
    zdefiniować metody statyczne dla grupy Zapisz i Wczytaj (na plik i z pliku). W obu klasach można dodać właściwości tylko do odczytu o ile
    jest to potrzebne. Napisz program ilustrujący. 
     * */
    class Program
    {
        static void Main(string[] args)
        {
            var g1 = new Grupa("Testowa");
            g1.DodajStudenta(5);
            g1.DodajStudenta(55);
            g1.DodajStudenta(555);
            g1.UsunStudenta(5);

            g1.DodajStudenta(6);
            g1.DodajStudenta(66);
            g1.DodajStudenta(666);

            Grupa.Zapisz(g1, "test.txt");

            var g2 = Grupa.Wczytaj("test.txt");

            Console.WriteLine(g1);
            Console.WriteLine(g2);
            Console.ReadKey();

        }
    }
}
