﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1
{
    class Grupa
    {
        private Student[] studenty;

        private string nazwa;
        private int Index = 0;
        public Grupa(string nazwa)
        {
            this.nazwa = nazwa;
            //Tworzymy tablice i rezerwujemy sobie miejsce w pamieci
            studenty = new Student[Int16.MaxValue];
        }
        public void DodajStudenta(int numerAlbumu)
        {
            studenty[Index] = new Student(numerAlbumu);
            Index++;
        }
        public void UsunStudenta(int numerAlbumu)
        {
            var konkretnyStudent = studenty.FirstOrDefault(x => x.NumerAlbumu == numerAlbumu);
            konkretnyStudent.IsDeleted = true;

        }
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(String.Format("{0} \t (", nazwa));
            foreach (var student in studenty.Where(x => x != null && x.IsDeleted == false))
            {
                sb.Append(String.Format("{0},", student.NumerAlbumu));
            }
            return sb.ToString().TrimEnd(',') + ")";
        }
        public static void Zapisz(Grupa grupa, string nazwaPliku)
        {
            StreamWriter file = new System.IO.StreamWriter(nazwaPliku);
            file.WriteLine(grupa.ToString());
            file.Close();
        }
        public static Grupa Wczytaj(string nazwaPliku)
        {
            StreamReader file = new System.IO.StreamReader(nazwaPliku);
            string myString = file.ReadLine();
            var mySplitString = myString.Split('\t');
            var tablicaStudentow = mySplitString[1].TrimStart(' ','(').TrimEnd(')',' ').Split(',');
            var GrupaNowa = new Grupa(mySplitString[0]);
            foreach (var item in tablicaStudentow)
            {
                GrupaNowa.DodajStudenta(Convert.ToInt32(item));
            }
            return GrupaNowa;
        }
    }
}
