﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1
{
    class Student
    {
        private int numerAlbumu;
        public Student(int numerAlbumu)
        {
            this.numerAlbumu = numerAlbumu;
        }
        public int NumerAlbumu
        {
            get
            {
                return numerAlbumu;
            }
        }
        public bool IsDeleted
        {
            get;set;
        }
        public override string ToString()
        {
            return String.Format("Student o numetrze {0}",numerAlbumu); 
        }
    }
}
