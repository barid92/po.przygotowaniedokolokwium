﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    class Program
    {
        /*
         
             Utwórz klasę SuperString, która zawiera metody rozszerzone (rozszerzające) dla klasy String: Odwroc(), Ile(char a) ile razy w stringu
występuje znak a , Skroc(in n) (skraca do stringu rozmiaru n – zostawia n-n/2 znakow na początku i n/2 na koncu), Usun(string s) (usunięcie
podstringu). Napisz program ilustrujący te metody.
*/
        static void Main(string[] args)
        {
            string tekst = "MojZajebistyTekst";
            Console.WriteLine(tekst.Odwroc());
            Console.WriteLine(tekst.Ile('j'));
            Console.WriteLine(tekst.Usun("Moj"));
            Console.ReadKey();
        }
    }
}
