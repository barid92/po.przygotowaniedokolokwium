﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    static class SuperString
    {
        public static string  Odwroc(this String s)
        {
            var sb = new StringBuilder();
            for (int i = s.Length-1; i >= 0; i--)
            {
                sb.Append(s[i]);
            }
            return sb.ToString();
        }
        public static uint Ile(this String s, char znak)
        {
            return Convert.ToUInt32(s.Where(x => x == znak).Count());
        }
        public static string Usun(this String s,string doUsuniecia)
        {
            return s.Replace(doUsuniecia, "");
        }
    }
}
