﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Kombajn : Maszyna
    {
        public Kombajn(uint moc, decimal cena, string opis) : base(moc, cena, opis)
        {
        }

        protected override string Typ
        {
            get
            {
                return "Kombajn";
            }
        }

        public override void Jedz()
        {
            Console.WriteLine("Sobota 100% Jedzie kumbajnem");
        }

        public override void Start()
        {
            Console.WriteLine("Sobota 100% Odpala kombajn");
        }

        public override void Stop()
        {
            Console.WriteLine("Sobota 100% Stopuje kumbajn");
        }
    }
}
