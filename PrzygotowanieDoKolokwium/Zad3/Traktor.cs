﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Traktor : Maszyna
    {
        public Traktor(uint moc, decimal cena, string opis) : base(moc, cena, opis)
        {
        }

        protected override string Typ
        {
            get
            {
                return "Traktor";
            }
        }

        public override void Jedz()
        {
            Console.WriteLine("Ram pam pam popek w traktorze jedzie");
        }

        public override void Start()
        {
            Console.WriteLine("Ram pam pam popek w traktorze odpala silnik");
        }

        public override void Stop()
        {
            Console.WriteLine("Ram pam pam popek w traktorze chamuje");
        }
    }
}
