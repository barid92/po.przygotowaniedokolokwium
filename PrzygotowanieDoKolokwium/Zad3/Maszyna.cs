﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    public abstract class Maszyna
    {
        private uint moc;
        private decimal cena;
        private string opis;
        private string typ;

        public Maszyna(uint moc, decimal cena,string opis)
        {
            this.moc = moc;
            this.cena = cena;
            this.opis = opis;
        }

        public virtual int Porownaj(Maszyna m)
        {
            if (m != null)
            {
                var mocDoPorownania = m.moc;
                if (this.moc == mocDoPorownania)
                {
                    return 0;
                }
                if (this.moc > mocDoPorownania)
                {
                    return 1;
                }
                if (this.moc > mocDoPorownania)
                {
                    return -1;
                }
            }
            throw new Exception("m is null");
        }
        public override string ToString()
        {
            return String.Format("{0} \t {1} \t {2} \t {3}", moc, cena, opis, Typ);
        }
        abstract public void Start();
        abstract public void Jedz();
        abstract public void Stop();
        abstract protected string Typ { get; }
    }
}
