﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Program
    {
        /*
         
             Utwórz klasę abstrakcyjną Maszyna zawierającą prywatne pola składowe moc, cena i opis, odpowiednie konstruktory, metody abstrakcyjne
Jazda(), Stop() i Start(), właściwość abstrakcyjną Typ (chroniona typu string który zawiera typ konkretnej maszyny np. traktor) oraz metody
string ToString() (metoda podaje dane maszyny w postaci stringu) i int Porownaj(Maszyna m).
abstract class Maszyna
{
 private int moc;
 private double cena;
 private string opis;
 // konstruktory
 public int Porownaj(Maszyna m)
 {
// tu napisz odpowiedni kod
 }
 public override string ToString()
 {
 // tu napisz odpowiedni kod
 }
 abstract public void Start();
 abstract public void Jedz();
 abstract public void Stop();
 abstract protected string Typ {get;}
}
Następnie zaimplementuj metodę Porownaj tak aby obiekty klasy Maszyna można było porównywać według mocy (zwraca 0 gdy moce są
równe, wartość ujemną gdy moc jest mniejsza niż porównywanej maszyny …). Utwórz klasy pochodne od klasy Maszyna: Kombajn i Traktor.
W klasach pochodnych dodaj dodatkowe pola składowe i metody oraz nowe wersje wymienionych metod abstrakcyjnych. Dodaj także
odpowiednie wersje metody ToString. Utwórz listę obiektów klasy Maszyna (park maszynowy) i umieść w niej obiekty klasy Kombajn i
Traktor. Zilustruj wszystko w przykładowym programie.*/
        static void Main(string[] args)
        {
            List<Maszyna> PakrMszyn = new List<Maszyna>();
            PakrMszyn.Add(new Traktor(55, 4566465, "Popkowy traktor"));
            PakrMszyn.Add(new Kombajn(34, 6666666, "Kombajn Soboty"));

            foreach (var maszynaMC in PakrMszyn)
            {
                maszynaMC.Start();
                maszynaMC.Jedz();
                maszynaMC.Stop();
            }
            
            Console.ReadKey();
        }
    }
}
